require('./config/config');
require('./dependency');

const express       = require('express');
const logger        = require('morgan');
const bodyParser    = require('body-parser');
const passport      = require('passport');

const v1 = require('./routes/v1'); // API version

const app = express();

app.use(logger(CONFIG.app));        // add loger
app.use(bodyParser.json());         // parse JSON
app.use(bodyParser.urlencoded({
    extended : false
}));                                //parse URL

//Pasport
// app.use(passport.initialize());

const models = require('./models');
models.sequelize.authenticate().then(() => {
    console.log('Let\'s try to look into the abyss ....');
    setTimeout(function () {
        console.log('find something SQL');
    }, 3000);
}).catch(err =>{
    console.error('I could not find anything. The abyss swallowed me. SQL dead.');
});

if (CONFIG.app === 'dev'){
    models.sequelize.sync(); //create tables from models
    console.log('find Models');
}

app.use(function (req, res, next) {
    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');
    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With, content-type, Authorization, Content-Type');
    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);
    // Pass to next layer of middleware
    next();
});
