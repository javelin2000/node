
let mysql=require('mysql');

const mc = mysql.createConnection({
    host     : CONFIG.db_host,
    port     : CONFIG.db_port,
    user     : CONFIG.db_user,
    password : CONFIG.db_password,
    database : CONFIG.db_name,
});
mc.connect(function(err) {
    if (err) {
        console.log('The abyss swallowed you request. SQL dead: ',err.sqlMessage );
        return false;
    }
    console.log('...find something horror... it is SQL');
});

module.exports=mc;