variable = require('./../env.json');
CONFIG = {};

CONFIG.app              = variable.APP  || 'dev';
CONFIG.port             = variable.PORT || '8000';

db_config =
CONFIG.db_dialect       = variable.DB_DIALECT    || 'mysql';
CONFIG.db_host          = variable.DB_HOST       || 'localhost';
CONFIG.db_port          = variable.DB_PORT       || '3306';
CONFIG.db_name          = variable.DB_NAME       || '';
CONFIG.db_user          = variable.DB_USER       || '';
CONFIG.db_password      = variable.DB_PASSWORD   || '';

CONFIG.JWT_ENCRYPTION      = variable.JWT_ENCRYPTION   || '';
CONFIG.JWT_EXPIRATION      = variable.JWT_EXPIRATION   || '';
