let Task = require('./../models/tasks');

let express = require('express');
let router = express.Router();

router.get('/:id?',function(req,res,next){
    console.log('The door to hell opens ... new GET request');
    if(req.params.id){
        Task.getTaskById(req.params.id,function(err,rows){
            if(err)
                {res.json(err);}
            else
                {res.json(rows);}
        });
    }else{
        Task.getAllTasks(function(err,rows){
            if(err)
            {res.json(err);}
            else
            {res.json(rows);}
        });
    }
});
router.post('/:id?', function (req, res, next) {
    if (req.query.hasOwnProperty('_method')){
        switch (req.query._method){
            case 'delete' :
                console.log('The door to hell opens ... new '+req.query._method +' request');
                Task.deleteTask(req.params.id, function (err, count) {
                    if (err)
                    {res.json(err);}
                    else
                    {res.json({
                        "Message"   : "Record with ID: "+count.insertId+" deleted",
                        "Error"     : false,
                        "Status"    : "Success"
                    })}
                });
                break
        }
    }else{
        Task.addTask(req.query, function (err, count) {
            console.log('The door to hell opens ... new POST request');
            if (err)
            {res.json(err);}
            else
            {res.json({
                "Message"   : "New records with ID: "+count.insertId+" added",
                "Error"     : false,
                "Status"    : "Success"
            })}
        })
    }
});

console.log('...find something .. it is Routes');
module.exports=router;