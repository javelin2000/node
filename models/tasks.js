db = require('./../config/db_connect'); //reference of dbcodb_connectnnection.js

let Task={

    getAllTasks:function(callback){
        return db.query("Select * from tasks", callback);
    },
    getTaskById:function(id,callback){
        return db.query("Select * from tasks where id=?", id,callback);
    },

    addTask:function(Task,callback){
        return db.query("INSERT INTO `tasks`(`title`, `status`) VALUES (?,?)",[Task.title,Task.status],callback);
    },
    deleteTask:function(id,callback){
        return db.query("delete from tasks where id=?",[id],callback);
    },
    updateTask:function(id,Task,callback){
        return db.query("update tasks set title=?,status=? where id=?",[Task.title,Task.status,id],callback);
    }

};
console.log('...find something... it is Models');

module.exports=Task;