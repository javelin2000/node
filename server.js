console.log('Let\'s try to look into the abyss ....');

require('./config/config');
require('./dependency')

const express       = require('express');
const app           = express();
const fs            = require('fs');
const path          = require('path');
const bodyParser    = require('body-parser');
const mysql         = require('mysql');
const passport      = require('passport');
const logger        = require('morgan');
const routes        = require('./routes/routes');

// create a write stream (in append mode)
let accessLogStream = fs.createWriteStream(path.join(__dirname, 'access.log'), {flags: 'a'});

// setup the logger
app.use(logger('combined', {stream: accessLogStream}));

// app.use(logger('dev'));
// app.use(passport.initialize());

require('./routes/routes');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended : true
}));

app.use('/', routes);


app.listen(CONFIG.port, function(){
    setTimeout(function () {
        console.log('Hell is waiting for you on port: '+CONFIG.port);
    },500);

});